![A.Wise Online Logo](logo.png)
# A.Wise Online Website
Source files for the [A.Wise Online website](https://awise.online).

## Versions
This version is 1.8.0 (1 January 2025).

The latest version can be found at [GitLab](https://gitlab.com/alanwise/alanwise.gitlab.io).

## Contributing
The project is hosted at [GitLab](https://gitlab.com/alanwise/alanwise.gitlab.io).

Ideas, bugs, etc., can be added to the issue tracker at [GitLab](https://gitlab.com/alanwise/alanwise.gitlab.io/issues) (registration required) or sent to me via [email](mailto:awisesoftware@gmail.com).

## Authors

* **Alan Wise** <awisesoftware@gmail.com> - Initial work.

## License
This file is part of awise.online.
Copyright (C) 2017-2025 by Alan Wise <awisesoftware@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

See [documentation/license.txt](documentation/license.txt) for details.
