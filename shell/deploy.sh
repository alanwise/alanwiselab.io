#!/bin/bash

# Install gems needed by project.
bundle install

# Build project.
bundle exec jekyll build -d public
