#!/bin/bash

# Install gems needed by project.
bundle install

# Install diagnostic gems.
npm install -g sass-lint
npm install -g stylelint
npm install -g stylelint-config-recommended --save-dev

# Run diagnostics gems
bundle exec jekyll doctor
sass-lint -v _sass/*
bundle exec jekyll build
npm init --yes
npm init stylelint --yes
stylelint --config stylelint-config-recommended "**/*.css"
